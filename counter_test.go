package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var (
	testStorageName = "test_storage.txt"
	hostURL         = "localhost:8070"
)

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	// only one handler; needs map if more handlers/routes in use
	// like map[url] = handler;
	RequestCounter(rr, req)
	return rr
}

func setup() {
	// use test storage for tests
	storagePath = testStorageName

}
func teardown() {
	// clear test storage
	os.Remove(testStorageName)

	// production storage back
	storagePath = "localStorage.txt"
}

// typicalCall: helper function that makes request and returns counterValue
func typicalCall(url, method string) (int, error) {
	req := httptest.NewRequest(method, url, nil)
	rr := executeRequest(req)
	resp := rr.Result()

	body, _ := ioutil.ReadAll(resp.Body)
	cValue, err := getCounterValue(string(body))
	return cValue, err
}

// TestNotZero: counting current request, counter can never be 0
func TestNotZero(t *testing.T) {
	setup()
	defer teardown()

	cValue, err := typicalCall(hostURL, "GET")

	if err != nil {
		t.Fatal(err)
	}
	if cValue < 1 {
		t.Fatalf("Got unacceptable counter value: %d", cValue)
	}
	return
}

//TestIncrement: checks that counter goes up
func TestIncrement(t *testing.T) {
	setup()
	defer teardown()

	// first value
	cValueFirst, err := typicalCall(hostURL, "GET")
	if err != nil {
		t.Fatal(err)
	}

	// second value
	cValueSecond, err := typicalCall(hostURL, "GET")
	if err != nil {
		t.Fatal(err)
	}

	if (cValueSecond - cValueFirst) != 1 {
		t.Fatalf("difference between number of consecutive must be 1, values: %d %d",
			cValueFirst, cValueSecond,
		)
	}
}

//TestStorageRestart: tests storage recreate functionality
func TestStorageRestart(t *testing.T) {
	setup()
	defer teardown()

	var (
		lastCounterValue int
		err              error
	)

	// make some requests number and take last counter value
	for i := 1; i <= 10; i++ {
		lastCounterValue, err = typicalCall(hostURL, "GET")
		if err != nil {
			t.Fatal(err)
		}
	}

	// reset storage
	resetURL := fmt.Sprintf("%s?restart=true", hostURL)
	// should be 1
	resetCounterValue, err := typicalCall(resetURL, "GET")
	if err != nil {
		t.Fatal(err)
	}

	//check values
	if lastCounterValue <= resetCounterValue {
		t.Fatalf("Reset storage failed, old value: %d; new value: %d", lastCounterValue, resetCounterValue)
	}

	if resetCounterValue != 1 {
		t.Fatalf("afetr storage reset, new value should be 1, instead: %d", resetCounterValue)
	}

}
