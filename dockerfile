FROM alpine

#ADD . usr/bin/request_counter
RUN apk add --no-cache \
        libc6-compat

WORKDIR usr/bin/request_counter

CMD ["./request_counter"]
