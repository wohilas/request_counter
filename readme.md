## Request Counter

Returns number of requests made for the last 60 seconds

### Technology in use
- Standard library in use only
- Storage in form of the text file(s)

#### reasons and the [KISS](https://en.wikipedia.org/wiki/KISS_principle) way to think:
- time condition on request counter makes one think about comparing time and using time types of data;
- persistent storage with standart library condition makes one to use some type of file;
- having those two components: writing timestamps to files is the simpliest solution to think about
(for example differnt one: would be to increment the counter on the request and then decrement the counter after 60 seconds,
but since it stil has to be re-written to a file, so it would create an extra step to think about)


### tests
current test situation:
```
$ go test -cover
PASS
coverage: 80.0% of statements
ok      gitlab.com/server_counter       0.005s
```

### docker suggestion (basically update_container.sh file)
don't forget to be in the repo folder
```
go build -o request_counter
docker build . -t r_counter

# if old conatiner exists
docker stop req_counter
docker rm req_counter

docker run -d -p 8070:8070 -v $(pwd):/usr/bin/request_counter --restart="always" --name="req_counter" r_counter
```