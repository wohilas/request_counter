package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

var storagePath = "localStorage.txt"

// RequestCounter handles requests
func RequestCounter(w http.ResponseWriter, req *http.Request) {

	//reset counter form check
	err := req.ParseForm()
	if err != nil {
		fmt.Println(err)
	}

	// if restart parameter given, delete storage file
	if val, ok := req.Form["restart"]; ok {
		// any value is true
		if len(val) > 0 {
			os.Remove(storagePath)
		}
	}

	// timestamp to write to storage
	strTime := time.Now().Format(time.RFC3339)

	// add current request to storage and get the counter value
	rNum := appendLineToFile(storagePath, strTime)

	msg := fmt.Sprintf("Number of calls for the last 60 seconds: %d", rNum)
	io.WriteString(w, msg)
}

func main() {
	http.HandleFunc("/", RequestCounter)
	http.ListenAndServe(":8070", nil)
}
