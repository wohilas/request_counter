go build -o request_counter
docker build . -t r_counter

# if old conatiner exists
docker stop req_counter
docker rm req_counter
docker run -d -p 8070:8070 -v $(pwd):/usr/bin/request_counter --restart="always" --name="req_counter" r_counter