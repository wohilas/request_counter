package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
)

// lineCounter filtering timestamps from storage using current time
// returns counter of timestamps with less then 60 second difference
// and list of those timestamps in string array
func lineCounter(filename string) (int, []string, error) {

	file, err := os.Open(filename)
	if err != nil {
		return 0, []string{}, err
	}

	currentTime := time.Now()
	lineCount := 0

	linesToWriteBack := []string{}

	// iterating through file lines to get relevant ones
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lineTime, err := time.Parse(time.RFC3339, line)
		if err != nil {
			fmt.Println(err)
			continue
		}
		timeDiff := currentTime.Sub(lineTime)
		if timeDiff < time.Second*60 {
			lineCount++
			linesToWriteBack = append(linesToWriteBack, line)
		}
	}

	return lineCount, linesToWriteBack, nil
}

// appendLineToFile adds a line to file in given path
// returns request counter
func appendLineToFile(filename, line string) int {

	// if storage file does not exists - create
	_, err := os.Stat(filename)
	if err != nil {
		_, err = os.Create(filename)
		if err != nil {
			// how?
			panic(err)
		}
	}

	// count lines with timestamps for last 60 secs
	result, linesToWrite, err := lineCounter(filename)

	if err != nil {
		panic(err)
	}

	// recreate file
	f, err := os.Create(filename)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	// after recreating the file only filtered/fresh lines-timestamps should be written
	linesToWrite = append(linesToWrite, line)
	for _, line := range linesToWrite {
		if _, err = f.WriteString(fmt.Sprintf("%s\n", line)); err != nil {
			panic(err)
		}
	}

	// after counting lines add current request to counter
	result++

	return result
}

// getCounterValue returns int counter from string body
func getCounterValue(line string) (int, error) {
	strCounter := strings.Split(line, ": ")[1]
	return strconv.Atoi(strCounter)
}
